// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.


function click(e) {
		chrome.tabs.query({'active': true, 'lastFocusedWindow': true}, function (tabs) {
		   var currentUrl = encodeURIComponent(tabs[0].url);
		   		if(e.target.id=="supportUrl")
					chrome.tabs.create({url:e.target.getAttribute('data-url') + "?url=" + currentUrl });
				else
			    	chrome.tabs.create({url:e.target.getAttribute('data-url')});
		});
}

document.addEventListener('DOMContentLoaded', function () {
  var divs = document.querySelectorAll('div');
  for (var i = 0; i < divs.length; i++) {
    divs[i].addEventListener('click', click);
  }
});

#   EUI Web Unit Chrome Extension for Contensis CMS
## A set of tools for the Web Working Group at the European University Institute.

### Usage ###

* Code and personalisation

* Changelog

* Branching and deployment instructions


### Who do I talk to? ###

* [EUI Web Unit](http://www.eui.eu/ServicesAndAdmin/CommunicationsService/WebUnit/People.aspx)


### Code and personalisation ###
The extension was developed to meet various requirements and needs of the EUI Web Working Group, a group of about 100 people working on the web at the European University Institute.
The EUI Web Unit chrome extension simplifies the management of the various sections of the [EUI Website](https://www.eui.eu) in Contensis, allowing users to quickly reach the page to edit within Contensis from the frontend, and allowing a fast and reliable cleaning of the cache (Varnish).

### -manifest.json ###
The manifest is a JSON-formatted file that describes the app and specifies a set of settings, such as the permissions and the path to the files.

In the manifest.json please include all the domains where the extension should have permissions.
Default:

```
#!json
    "permissions": [
    "http://*.eui.eu/*", "https://preview-eui.contensis.com/*", "activeTab"
    ],


```

### -popup.html and popup.js ###
These files specify the behaviour of the extension Button in the Chrome toolbar.
By default the EUI Web Unit Chrome Extension shows a list of useful links for the EUI Web Working Group. It's easy to edit both the files to meet your needs.

### -EUIscript.js ###
This file contains the main logic of the extension, and it's in pure js (you could include external libraries, if needed).



**Edit this page Button**

```
#!javascript

if((location.hostname.match('eui.eu')|| location.hostname.match('preview-eui.contensis.com') ) && checkifContensis!=null ){

```
This conditional instruction checks the domain to show the buttons. By default, the extension show the buttons only in the EUI website, both in live and preview.


```
#!javascript

var metas = document.getElementsByTagName('meta'); 
var contentid = null;
   for (i=0; i<metas.length; i++) { 
      if (metas[i].getAttribute("name") == "CONTENT.ID") { 
         contentid = metas[i].getAttribute("content"); 
      } 
   }

```

This piece of code is fundamental to compose the final url for the "Edit this page" button. Different version of Contensis might require different checks for the CONTENT ID. The code that works for Contensis 8.1 and before is this part:


```
#!javascript
//Contensis 8.1 and below
var metas = document.getElementsByTagName('meta'); 
var contentid = null;
   for (i=0; i<metas.length; i++) { 
      if (metas[i].getAttribute("name") == "CONTENT.ID") { 
         contentid = metas[i].getAttribute("content"); 
      } 
   }

//Contensis 8.2 and above
if(contentid==null){
var contentIDclass= checkifContensis.className.substring(0, checkifContensis.className.indexOf(' '));
 contentid = contentIDclass.substr(contentIDclass.indexOf("-") + 1);
}

```
 
Since version 8.2, Contensis does not output by default the meta tag with the Content ID, so you will need to include the Content ID somewhere in your Contensis template. 
To retrieve in js the Content ID for version 8.2 and above, in our environment, we set this piece of code in the Base template, which outputs in the base form a few useful classes: the page id, the folder ID and the top folder ID.

```
#!.NET

Dim formwrapper As HtmlForm= CType(Page.FindControl("form1"), HtmlForm)
      If Not (formwrapper Is Nothing) Then
        formwrapper.Attributes.Add("class", "pageID-" & CurrentNode.Data.C_ID & " folderID-" & CurrentNode.Data.F_ID  & " topFolderID-" & CurrentNode.Data.TopFolderID )
End If

```

Finally, we can generate the Edit button url, which is hard coded here:

```
#!javascript

btn.addEventListener("click", 	function(){
		url = "https://cms-XXXXX.contensis.com/?Deeplink=79&DialogueKey_ContentID="+ contentid +"&DialogueKey_ContentTypeID=0&DialogueKey_WorkflowType=2";
		var win = window.open(url, '_blank');
		win.focus();
	}

```
Simply change cms-XXXXX.contensis.com with the url of your CMS.

**Clear the cache Button**

This button works with EUI Varnish caching configuration. Please contact us if you need some advice on how to configure the script for purging the cache from the frontend.

To hide this button simply comment out all the references to btn2.

# Changelog #
## Version 2.0 Released ##
Release date: 4 August 2016

* New design

* Tag manager fired inside the CMS (analytics, metric, alerts and customisation on Contensis backend)


# Branching and deployment instructions #
You are welcome to branch this project, and propose any addons, modifications or integrations.

To deploy the extension to your users, you are required to have a [Google Chrome developer account](https://chrome.google.com/webstore/developer/dashboard?hl=it)

You can find the unlisted EUI extension on the Chrome web store -> [Here](https://chrome.google.com/webstore/detail/eui-webunit-extension/cjdinkhhhnmnbkibjcchajpgefbijilk?hl=it)